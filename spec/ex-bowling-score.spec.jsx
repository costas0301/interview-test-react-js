import React from 'react/addons';
import ExBowlingScore from '../lib/components/App.jsx';

describe('ExBowlingScore', function() {
  var component;

  beforeEach(function() {
    component = React.addons.TestUtils.renderIntoDocument(
      <ExBowlingScore/>
    );
  });

  it('should render', function() {
    expect(component.getDOMNode().className).toEqual('ex-bowling-score');
  });
});
