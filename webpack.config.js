/* jshint node: true */
var path = require('path');


module.exports = {
  context: path.join(__dirname),
  entry: './lib/index.js',

  output: {
    path: path.join(__dirname, 'dist'),
    filename: "bundle.min.js",
    publicPath: '/static/'
  },

  module: {
    loaders: [
      {
        test: /\.scss$/,
        // Query parameters are passed to node-sass
        loader: 'style!css!sass?outputStyle=expanded&' +
          'includePaths[]=' + (path.resolve(__dirname, './bower_components')) + '&' +
          'includePaths[]=' + (path.resolve(__dirname, './node_modules'))
      },
      {
        test: /(\.js)|(\.jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        include: path.join(__dirname, 'lib'),
        query: {
          presets: ['react', 'es2015', 'stage-0']
        }
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    port: 9999,
    proxy: {
      '/rest/**': {
        target: 'http://localhost:8090/rest',
        secure: false,
        rewrite: function(req) {
          req.url = req.url.replace(/^\/rest/, '');
        },
        changeOrigin: true
      }
    }
  }

};
