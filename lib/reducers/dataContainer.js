export function getNewState() {
  return {
    currentRoll: '',
    rolls: [],
    frames: []
  }
}

export function dataContainer(state = {}, action) {
  switch (action.type) {
    case 'NEW':
      return getNewState();
    case 'CHANGE':
      return {...state, currentRoll: action.pins};
    case 'THROW_BALL':
      return {
        rolls: [...state.rolls, action.pins],
        frames: action.frames,
        currentRoll: ''
      }
    default:
      return state;
  }
}

