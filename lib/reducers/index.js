import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import {dataContainer} from './dataContainer';

const reducer = combineReducers({
	dataContainer, form: formReducer
});

export default reducer;
