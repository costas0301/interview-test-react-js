import React from 'react';
import ReactDOM from 'react-dom';
import AppController from './controllers/AppController.jsx';
import { Provider } from 'react-redux';
import { store } from './store';

const app = (
  <Provider store={ store }>
    <AppController/>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
