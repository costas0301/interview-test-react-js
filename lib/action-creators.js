export function newGame() {
  return {
    type: 'NEW'
  };
}
export function change(pins) {
  return {
    type: 'CHANGE',
    pins
  };
}

export function throwBall(pins, frames) {
  return {
    type: 'THROW_BALL',
    pins,
    frames
  };
}

