import {createStore} from 'redux';
import rootReducer from './reducers';
import {getNewState} from './reducers/dataContainer'

const store = createStore(rootReducer, {dataContainer: getNewState()});

//if (module.hot) {
//  module.hot.accept('./reducers/', () => {
//    const nextRootReducer = require('./reducers').default;
//
//    store.replaceReducer(nextRootReducer);
//  });
//}

export {store};
