import React from 'react';
import {Form, FormGroup, Row, Col, FormControl, Navbar, Button } from 'react-bootstrap';

const emptyFrame = {
  firstChancePinFall: '',
  lastChancePinFall: '',
  frameClosed: true,
  score: '',
  strike: false,
  spare: false
}

class Pins extends React.Component {
  render() {
    const { additionalRoll} = this.props;
    const pinsGridStyle = {
      color: 'orange',
      borderRight: this.props.border ? "solid 1px" : "",
      width: additionalRoll ? '33%' : '50%',
      height: '100%',
      display: 'inline-block',
      background: !additionalRoll && this.props.strike ? 'darkblue' : this.props.spare ? 'linear-gradient(to right bottom, white 50%, darkblue 50%)' : '',
      float: 'left'
    };
    return (
      <div style={pinsGridStyle}>
        {this.props.pins}
      </div>
    )
  }
}

class Score extends React.Component {
  render() {
    const scoresStyle = {
      position: 'absolute',
      color: 'orange',
      borderBottom: "solid 1px",
      width: 50,
      height: 25,
      vAlign: "top",
      display: 'inline-block',
      textAlign: 'center'
    };

    const {frame, additionalRoll} = this.props;
    const additionalRollPins = additionalRoll ? <Pins border pins={frame.additionalChancePinFall} additionalRoll={additionalRoll}/> : null;
    return (
      <div style={scoresStyle}>
        <Pins border pins={frame.firstChancePinFall} additionalRoll={additionalRoll} />
        <Pins
          pins={frame.lastChancePinFall === 10 || frame.frameClosed && !frame.spare? frame.lastChancePinFall : ''}
          strike={frame.strike}
          spare={frame.spare}
          border={additionalRoll}
          additionalRoll={additionalRoll}
        />
        {additionalRollPins}
        {frame.score}
      </div>
    )
  }
}

class FrameComponent extends React.Component {
  render() {
    const frameBoxStyle = {
      color: 'orange',
      border: "solid 1px",
      width: 50,
      height: 50,
      display: 'inline-block'
    };
    return (
      <div style={frameBoxStyle}>
        <Score frame={this.props.frame} additionalRoll={this.props.additionalRoll}/>
      </div>
    )
  }
}

class GameComponent extends React.Component {
  render() {
    var elements = [];
    for (let i = 0; i < 10; i++) {
      elements.push(<FrameComponent key={i} frame={this.props.frames[i] || emptyFrame} additionalRoll={i === 9}/>);
    }
    return (<div>
        {elements}
      </div>
    )
  }
}


export default class App extends React.Component {
  render() {
    const currentRollIsValid = this.props.validate(this.props.store.currentRoll);
    let elements = [];
    for (let i = 0; i < this.props.helper.getMaxPinsInFrame() + 1; i++) {
      elements.push(<Button onClick={e => this.props.actions.throwBallOnButtons(i)} key={i}>{i}</Button>);
    }
    return (
      <div className="container">
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              Roll
            </Navbar.Brand>
          </Navbar.Header>
          <Navbar.Collapse>
            <Navbar.Form pullLeft>
              <FormGroup validationState={currentRollIsValid ? 'success' : 'error'}>
                <FormControl type="text" placeholder="Roll" value={this.props.store.currentRoll} onChange={this.props.actions.change}/>
                <FormControl.Feedback />
              </FormGroup>
              {' '}
              <Button onClick={this.props.actions.throwBall} disabled={!currentRollIsValid}>Throw</Button>
              <Button onClick={this.props.actions.newGame}>New Game</Button>
              {elements}
            </Navbar.Form>
          </Navbar.Collapse>
        </Navbar>

        <Row>
          <Col sm={6}>
            <GameComponent frames={this.props.store.frames}/>
          </Col>
        </Row>
      </div>
    )
  }
};
