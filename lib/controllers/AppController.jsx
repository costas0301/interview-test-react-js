import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import location from 'location-href';
import axios from "axios";
import {newGame, throwBall, change} from '../action-creators';
import App from '../components/App.jsx';

function getCurrentPathname() {
  const path = location();
  return path.charAt(path.length - 1) == '/' ? path : path + "/";
}

class AppController extends React.Component {
  throwBall = e => {
    this.throwBallOnButtons(this.props.dataContainer.currentRoll)
  }

  throwBallOnButtons = pins => {
    const rolls = [...this.props.dataContainer.rolls, pins];
    const url = getCurrentPathname() + 'rest/frames';
    axios.post(url, rolls).then(res => {
      if (res.status === 200) {
        this.props.throwBall(pins, res.data);
      }
    });
  }

  change = e => {
    this.props.change(e.target.value);
  }

  validate = (pins) => {
    return /^(\d+?)?$/.test(pins) && parseInt(pins) <= this.getMaxPinsInFrame();
  }

  getLastFrame = () => {
    const {frames} = this.props.dataContainer;
    if (!frames.length) {
      return {
        firstChancePinFall: 0,
        lastChancePinFall: 0
      }
    }
    return frames[frames.length - 1];
  }

  getMaxPinsInFrame = () => {
    const frame = this.getLastFrame();
    let maxPins = 10 - (frame.firstChancePinFall + frame.lastChancePinFall);
    if (frame.length === 10 || maxPins > 0 && !frame.frameClosed) {
      return maxPins;
    }
    return 10;
  }


  render() {
    return <App
      actions={{
      throwBall : this.throwBall,
      newGame: this.props.newGame,
      change : this.change,
      throwBallOnButtons: this.throwBallOnButtons}}
      store={this.props.dataContainer}
      validate={this.validate}
      helper={{getMaxPinsInFrame : this.getMaxPinsInFrame}}
    />
  }
}


export default connect(
  store => store,
  dispatch => bindActionCreators({
    newGame,
    throwBall,
    change
  }, dispatch)
)(AppController);

